%% Class dedicated to the initialisation, the measure and the display of the spent time
%% L.LAURENT -- 15/05/2012 -- luc.laurent@lecnam.net

% MIT License
% 
% Copyright (c) 2019 Luc LAURENT (luc.laurent@lecnam.net)
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

classdef mesuTime < handle
    properties (Access = private)
        counterTicToc=[];
        tInit=[];
        tFinal=[];
        funDisp='fprintf';
    end
    properties
        tElapsed=[];
        tCPUElapsed=[];
    end
    methods
        %constructor
        function obj=mesuTime
            obj.start;
        end
        %start the measure
        function start(obj)
            %initialization tic-toc count
            obj.counterTicToc=tic;
            %Measure initial CPU time
            obj.tInit=cputime;
        end
        %stop the measure
        function stop(obj,flagDisp)
            if nargin==1;flagDisp=true;end
            %Elapsed CPU time
            obj.tElapsed=toc(obj.counterTicToc);
            %Elapsed time
            obj.tCPUElapsed=cputime-obj.tInit;
            %show the result
            if flagDisp
                show(obj);
            end
        end
        %get CPU time elasped
        function timeElapsed=cpuElapsed(obj)
            if isempty(obj.tCPUElapsed);obj.stop(false);end
            timeElapsed=obj.tCPUElapsed;
        end
        %get time elasped
        function timeElapsed=Elapsed(obj)
            if isempty(obj.tCPUElapsed);obj.stop(false);end
            timeElapsed=obj.tElapsed;
        end
        %display the elapsed time
        function show(obj)
            feval(obj.funDisp,'  #### Time/CPU time (s): %4.2f s / %4.2f s\n',obj.tElapsed,obj.tCPUElapsed);
        end
    end
end
