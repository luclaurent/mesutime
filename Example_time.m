% Example of use of the mesuTime class
% L. LAURENT -- 25/02/2019 -- luc.laurent@lecnam.net

%create object
objectTime=mesuTime;
%start new measure with
% objectTime.start;


%run many things
pause(5);
%obtain CPU time
objectTime.cpuElapsed
%obtain elapsed time
objectTime.Elapsed
%show result
objectTime.show;

